---
title: "Rapport projet fouille de données"
author: "YESSOUFOU Ibtissamath BELHABIB Fatma"
date: "2024-04-27"
output: html_document
---

# Introduction
L'analyse s'effectue sur un jeu de donnée de  de l'Enquête nationale indonésienne sur la prévalence de la contraception de 1987.Le dataset, est composé de 1473 et 9 variables sans compter le type de contraception, décrivant le statut démographique et socio-économique des sujets.
Le but est de construire un modèle d'apprentissage automatique efficace pour prédire le choix actuel de méthode contraceptive.


# Analyses preliminaires
Nous avons tout d'abord effectué des analyses préliminaires pour explorer le jeu de donnée.Une transformation du jeu de donnée brut à d'abord été effectué pour attribuer aux variables et à leur modalités les noms correspondants.Deplus nous avons transformé les variables numériques "NbEnfants"et "AgeF" en variables catégorielles et réprtissant par intervalles (cf script R).Nous avons ainsi ,dans un premier temps, obtenu une matrice.


```r
matrice=read.csv("matrice.csv")
```

```
## Warning in file(file, "rt"): impossible d'ouvrir le
## fichier 'matrice.csv' : No such file or directory
```

```
## Error in file(file, "rt"): impossible d'ouvrir la connexion
```

```r
summary(matrice)
```

```
##  Contraception       EducationF       
##  Length:1473        Length:1473       
##  Class :character   Class :character  
##  Mode  :character   Mode  :character  
##   EducationH         ReligionF        
##  Length:1473        Length:1473       
##  Class :character   Class :character  
##  Mode  :character   Mode  :character  
##    TravailF           TravailH        
##  Length:1473        Length:1473       
##  Class :character   Class :character  
##  Mode  :character   Mode  :character  
##  indiceniveauvie       Media          
##  Length:1473        Length:1473       
##  Class :character   Class :character  
##  Mode  :character   Mode  :character  
##     Nb_int            AgeF_int        
##  Length:1473        Length:1473       
##  Class :character   Class :character  
##  Mode  :character   Mode  :character
```

Au vu du summary, il n'y a pas de données manquantes ou inconnues et toutes les variables semblent apporter une information.





```r
x=matrice[,-1]
y=matrice[,1]
```


```r
sapply(colnames(x), function(a) chisq.test(x=x[,a], y=y)$p.value)
```

```
##      EducationF      EducationH       ReligionF 
##    8.018770e-28    6.304537e-14    2.017715e-05 
##        TravailF        TravailH indiceniveauvie 
##    7.475199e-02    3.572620e-12    1.606981e-11 
##           Media          Nb_int        AgeF_int 
##    1.393689e-07    5.795429e-36    7.006111e-19
```

![Table représentant les catégories de méthodes contraceptives selon le nombre d'enfants](nb_enf.png)


![Table représentant les catégories de méthodes contraceptives selon l'age](age.png)

Le test de chi2 est significatif pour chacune des variables. Il y a donc une liaison entre la classe et chacune des variables.Cependant, lorsque l'on observe les shémas des tables decontingence, les modalité pour la plupart ne semblent pas toutes pouvoir bien trancher entre les différents catégories de contraception. On peut quand même voir de nettes différences pour les **parents ayant 0 enfants** et une certaines tendances en fonction de **l'âge**.


# Etape de classification automatique

Nous avons décidé d'utiliser Knime workflow pour la suite de la classification.


Dans ce projet, nous avons entrepris une classification automatique sur notre jeu de données avec la classe "contraception" déjà prédéfinie. Cela nous permet de mener une classification supervisée, où la classe est déjà connue. Nous avons utilisé KNIME workflow et décidé de faire varier plusieurs paramètres pour trouver le meilleur modèle, c'est-à-dire celui avec le moins d'erreurs possible, notre objectif étant d'avoir un taux d'erreur inférieur à 50%.

Nous avons choisi de coupler cette classification à une validation croisée. Pour cela, nous avons opté pour une validation croisée à 10-fold(nombre de validation=10), ce qui signifie que nous avons partitionné notre jeu de données en 10 parties égales. Nous avons utilisé 9/10 des données pour l'apprentissage du modèle et 1/10 pour le test, ce qui nous a donné 10 modèles et 10 résultats de performance. La moyenne de ces résultats nous a permis d'évaluer la performance globale des méthodes et des paramètres.

Nous avons commencé par utiliser l'arbre de décision avec différents paramètres :
- Nombre de validations : 20
- Random sampling(Échantillonnage aléatoire)
- No prunning(Aucun élagage)
- Indice de Gini

Cela nous a donné une erreur moyenne de 52.819%, ce qui est au-dessus de notre seuil de 50%. Nous avons ensuite ajusté les paramètres :
- Nombre de validations : 10
- Random sampling(Échantillonnage aléatoire)
- MDL (Minimum Description Length) pour l'élagage
- Indice de Gini

Cela a réduit l'erreur moyenne à 48.34%, mais elle reste élevée. Nous avons ensuite filtré certaines colonnes :
- Nous avons filtrer(exclues) les colonnes Religion, TravailF, TravailH et Indicevie

Cela a légèrement amélioré l'erreur moyenne à 46.36%. Malgré plusieurs essais avec différents ajustements de paramètres, nous n'avons pas réussi à réduire davantage l'erreur moyenne.

Nous avons exploré d'autres méthodes pour améliorer notre  classification. 
Avec le modèle bayésien naïf couplé à une validation croisée, nous avons utilisé les paramètres par défaut et obtenu une erreur moyenne de 100%. Par la suite, nous avons envisagé d'utiliser notre propre prédicteur bayésien, que nous avions implémenté lors du TP4. En appliquant ce prédicteur, nous avons constaté que la performance du modèle était de 57.336%, ce qui représente une erreur moyenne moins bonne que celle obtenue avec les arbres de décision. 


![Erreur moyenne du classificateur bayesien naif](bayesiennaif.png)

Malheureusement, le classificateur bayésien naïf n'a pas permis d'améliorer nos performances.
De même, avec KNIME, nous avons appliqué une forêt aléatoire avec échantillonnage des attributs pour décorrélé les arbres construits, couplée à une validation croisée. En utilisant les paramètres par défaut, nous avons également obtenu une erreur moyenne de 100%.


Après avoir testé différents modèles (arbres de décision, forêt aléatoire, Bayésien naïf) et paramètres (k-fold validation, coefficient de GINI...), la plus faible erreur moyenne obtenue était de 46.36% en filtrant certains attributs. Cependant, en prenant en compte tous les attributs, l'erreur moyenne était de 48.34%, toujours inférieure à 50%. Peut-être devrions-nous envisager l'utilisation d'une méthode plus adaptée à notre jeu de données. Par exemple, nous pourrions tester la méthode des réseaux de neurones, que nous couplerions avec une validation croisée. Étant donné que les réseaux de neurones peuvent être très puissants pour la classification, il est possible que cela améliore l'erreur moyenne.






