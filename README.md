# Datamining


## Name
Prédiction du choix de la méthode contraceptive

## Description
Ce jeu de données est un sous-ensemble de l'Enquête nationale indonésienne sur la prévalence de la contraception de 1987. Les échantillons sont des femmes mariées qui n'étaient pas enceintes ou qui ne savaient pas si elles l'étaient au moment de l'entretien.Le dataset, est composé de 1473 et 9 variables sans compter le type de contraception, décrivant le statut démographique et socio-économique des sujets.

## Objectives
L'objectif est de construire un modèle d'apprentissage automatique efficace pour prédire le choix actuel de méthode contraceptive (non-utilisation, méthodes à long terme ou méthodes à court terme) d'une femme, en se basant sur ses caractéristiques démographiques et socio-économiques.
Pour ce faire, le  jeu de données sera divisé en ensembles d'apprentissage et de test, en utilisant ⅔ des données pour l'entraînement du modèle et ⅓ pour l'évaluation de sa performance.
Afin d'établir le modèle,il s’agira dans un premier temps de parcourir les données afin de déterminer les facteurs majeurs qui influencent les choix de contraception dans les couples,dans un deuxième temps de faire le choix de l’algorithme le plus adapté pour le modèle et enfin d’évaluer et valider le modèle optimal.




